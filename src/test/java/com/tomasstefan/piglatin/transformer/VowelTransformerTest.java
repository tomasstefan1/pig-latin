package com.tomasstefan.piglatin.transformer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class VowelTransformerTest {

    private static Stream<Arguments> argumentsProvider() {
        return Stream.of(
                Arguments.of("a", "away"),
                Arguments.of("Hello", "Hello"),
                Arguments.of("away", "away"),
                Arguments.of("aB", "aBway")
        );
    }

    @ParameterizedTest
    @MethodSource("argumentsProvider")
    void transform(String input, String expected) {
        VowelTransformer transformer = new VowelTransformer();
        String result = transformer.transform(input);
        assertEquals(expected, result);
    }

}