package com.tomasstefan.piglatin.transformer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class HyphenTest {

    private static Stream<Arguments> argumentsProvider() {
        return Stream.of(
                Arguments.of("a-a", "apig-apig"),
                Arguments.of("aa", "aapig")
        );
    }

    @ParameterizedTest
    @MethodSource("argumentsProvider")
    void transform(String input, String expected) {
        HyphenTransformer transformer = new HyphenTransformer(text -> text + "pig");
        String result = transformer.transform(input);
        assertEquals(expected, result);
    }
}