package com.tomasstefan.piglatin.transformer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class CharacterCaseHeatMapTest {

    private static Stream<Arguments> heatMapProvider() {
        return Stream.of(
                Arguments.of("aa", "BB", "bb"),
                Arguments.of("AA", "b", "B"),
                Arguments.of("Hello-world", "ellohay-orldway", "Ellohay-orldway"),
                Arguments.of("Hello", "ellohay", "Ellohay")
        );
    }

    @ParameterizedTest
    @MethodSource("heatMapProvider")
    void transform(String input, String transformationInput, String expected) {
        CharacterCaseHeatMap heatMap = new CharacterCaseHeatMap(input);
        String result = heatMap.transform(transformationInput);
        assertEquals(expected, result);
    }
}