package com.tomasstefan.piglatin.transformer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class PunctuationTransformerTest {

    @ParameterizedTest
    @MethodSource("transformProvider")
    void transform(String input, String transformationInput, String expected) {
        PunctuationTransformer transformer = new PunctuationTransformer(input);
        String result = transformer.transform(transformationInput);
        assertEquals(expected, result);
    }

    @ParameterizedTest
    @MethodSource("getWithoutPunctuationProvider")
    void getWithoutPunctuation(String input, String expected) {
        PunctuationTransformer transformer = new PunctuationTransformer(input);
        assertEquals(expected, transformer.getWithoutPunctuation());
    }

    private static Stream<Arguments> transformProvider() {
        return Stream.of(
                Arguments.of("aa.", "bb", "bb."),
                Arguments.of("a'a", "bbb", "bb'b"),
                Arguments.of("it?", "itway", "itway?"),
                Arguments.of("a`a,a.a!a?a\"a'a:a", "bbbbbbbbb", "b`b,b.b!b?b\"b'b:b"),
                Arguments.of("a:abc", "a", ":a")
        );
    }

    private static Stream<Arguments> getWithoutPunctuationProvider() {
        return Stream.of(
                Arguments.of("`aa.", "aa"),
                Arguments.of("a'a", "aa"),
                Arguments.of("it?", "it"),
                Arguments.of("a`a,a.a!a?a\"a'a:a(a)a[a]a{a}a", "aaaaaaaaaaaaaaa"),
                Arguments.of("a:abc", "aabc")
        );
    }
}