package com.tomasstefan.piglatin.transformer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class CombinedTransformerTest {

    private static Stream<Arguments> argumentsProvider() {
        return Stream.of(
                Arguments.of("Hello", "Ellohay"),
                Arguments.of("apple", "appleway"),
                Arguments.of("stairway", "stairway"),
                Arguments.of("can't", "antca'y"),
                Arguments.of("end.", "endway."),
                Arguments.of("this-thing", "histay-hingtay"),
                Arguments.of("Beach", "Eachbay"),
                Arguments.of("McCloud", "CcLoudmay"),
                Arguments.of("(yet)?", "et(yay)?"),
                Arguments.of("3word", "3word"), //no transformation for numbers
                Arguments.of("3", "3") //no transformation for numbers
        );
    }

    @ParameterizedTest
    @MethodSource("argumentsProvider")
    void transform(String input, String expected) {
        var transformer = new CombinedTransformer();
        var actual = transformer.transform(input);
        assertEquals(expected, actual);
    }

}