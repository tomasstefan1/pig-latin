package com.tomasstefan.piglatin.transformer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class ConsonantTransformerTest {

    private static Stream<Arguments> argumentsProvider() {
        return Stream.of(
                Arguments.of("aa", "aa"),
                Arguments.of("Hello", "Ellohay"),
                Arguments.of("w", "way")
        );
    }

    @ParameterizedTest
    @MethodSource("argumentsProvider")
    void transform(String input, String expected) {
        ConsonantTransformer transformer = new ConsonantTransformer();
        String result = transformer.transform(input);
        assertEquals(expected, result);
    }

}