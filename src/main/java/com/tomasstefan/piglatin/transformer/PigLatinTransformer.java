package com.tomasstefan.piglatin.transformer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>Translates a string (word, sentence, or paragraph) into “pig-latin” using the  following rules.</p>
 * <br/>
 * <ul>
 *     <li>Words that start with a consonant have their first letter moved to the end of the word, and the  letters “ay” added to the end.
 *         <ul>
 *             <li><b>Hello</b> becomes <b>Ellohay</b></li>
 *         </ul>
 *     </li>
 *     <li>Words that start with a vowel have the letters “way” added to the end.
 *         <ul>
 *             <li><b>apple</b> becomes <b>appleway</b></li>
 *         </ul>
 *     </li>
 *     <li>Words that end in “way” are not modified.
 *         <ul>
 *             <li><b>stairway</b> stays as <b>stairway</b></li>
 *         </ul>
 *     </li>
 *     <li>Punctuation must remain in the same relative place from the end of the word.
 *         <ul>
 *             <li><b>can’t</b> becomes <b>antca’y</b></li>
 *             <li><b>end.</b> becomes <b>endway.</b></li>
 *         </ul>
 *     </li>
 *     <li>Hyphens are treated as two words
 *         <ul>
 *             <li><b>this-thing</b> becomes <b>histay-hingtay</b></li>
 *         </ul>
 *     </li>
 *     <li>Capitalization must remain in the same place.
 *         <ul>
 *             <li><b>Beach</b> becomes <b>Eachbay</b></li>
 *             <li><b>McCloud</b> becomes <b>CcLoudmay</b></li>
 *         </ul>
 *     </li>
 * </ul>
 */
public class PigLatinTransformer implements ITextTransformer {
    private static final Pattern WHIT_SPACE = Pattern.compile("(\\s+)");

    @Override
    public String transform(String input) {
        if (input == null) throw new IllegalArgumentException("input cannot be null");
        if (input.isBlank()) return input;
        input = input.trim();
        CombinedTransformer combinedTransformer = new CombinedTransformer();
        Matcher matcher = WHIT_SPACE.matcher(input);
        String[] split = WHIT_SPACE.split(input);
        StringBuilder builder = new StringBuilder();
        for (String text : split) {
            builder.append(combinedTransformer.transform(text));
            if (matcher.find()) {
                builder.append(matcher.group(0));
            }
        }
        return builder.toString();
    }

}
