package com.tomasstefan.piglatin.transformer;

class VowelTransformer extends AbstractTextTransformer {
    @Override
    protected String performTransformation(String input) {
        return input + "way";
    }

    @Override
    protected boolean canPerformTransformationImpl(String input) {
        return isVowel(input.charAt(0));
    }

}
