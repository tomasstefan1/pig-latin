package com.tomasstefan.piglatin.transformer;

@FunctionalInterface
public interface ITextTransformer {
    /**
     * Perform certain type of text transformation and returns the result
     * @param input
     * @return transformed input
     */
    String transform(String input);

    default boolean canPerformTransformation(String input) {
        return true;
    }
}
