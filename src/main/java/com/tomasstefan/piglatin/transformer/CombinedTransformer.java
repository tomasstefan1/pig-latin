package com.tomasstefan.piglatin.transformer;

/**
 * Combined transformers that applies all rules to the word transformation.
 * Cannot be used for complex text transformation as it treats whitespaces as regular characters that are part of the word.
 * <br/>
 * Applies first {@link HyphenTransformer}
 */
class CombinedTransformer implements ITextTransformer {
    private static final ConsonantTransformer CONSONANT_TRANSFORMER = new ConsonantTransformer();
    private static final VowelTransformer VOWEL_TRANSFORMER = new VowelTransformer();
    private final HyphenTransformer transformer = new HyphenTransformer(combined());
    @Override
    public String transform(String input) {
        return transformer.transform(input);
    }

    /**
     * Perform all available transformation per word
     * @return combined transformer for a single word
     */
    private static ITextTransformer combined() {
        return input -> {
            if (CONSONANT_TRANSFORMER.canPerformTransformation(input)) {
                return CONSONANT_TRANSFORMER.transform(input);
            } else if (VOWEL_TRANSFORMER.canPerformTransformation(input)) {
                return VOWEL_TRANSFORMER.transform(input);
            }
            return input;
        };
    }
}
