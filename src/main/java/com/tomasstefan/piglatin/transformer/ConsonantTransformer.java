package com.tomasstefan.piglatin.transformer;

public class ConsonantTransformer extends AbstractTextTransformer {

    @Override
    protected String performTransformation(String input) {
        char[] chars = input.toCharArray();
        int length = chars.length;
        char[] result = new char[length + 2];
        for (int i = 1; i < length; i++) {
            result[i-1] = chars[i];
        }
        result[length -1] = chars[0];
        result[length] = 'a';
        result[length+1] = 'y';

        return String.valueOf(result);
    }

    @Override
    protected boolean canPerformTransformationImpl(String input) {
        return isConsonant(input.charAt(0));
    }
}
