package com.tomasstefan.piglatin.transformer;

/**
 * Preserves both case sensitivity and punctuation of transforming word.
 */
abstract class AbstractTextTransformer implements ITextTransformer {

    @Override
    public final String transform(String input) {
        if (input == null || input.isBlank()) throw new IllegalArgumentException("Input cannot be null");
        if (input.endsWith("way") || !canPerformTransformation(input)) return input;
        var heatMap = new CharacterCaseHeatMap(input);
        var punctuationTransformer = new PunctuationTransformer(input.toLowerCase());
        return heatMap.transform(punctuationTransformer.transform(performTransformation(punctuationTransformer.getWithoutPunctuation())));
    }

    /**
     * Input is provided always with lower case and correct upper/lower case is assigned later after performing transformation.
     * @param input that is not null and lower case without punctuation
     * @return transformed input
     */
    protected abstract String performTransformation(String input);

    @Override
    public final boolean canPerformTransformation(String input) {
        if (input == null || input.isBlank()) throw new IllegalArgumentException("Input cannot be null");
        var punctuationTransformer = new PunctuationTransformer(input);
        return canPerformTransformationImpl(punctuationTransformer.getWithoutPunctuation());
    }

    /**
     * @param input not null and without punctuation
     * @return <code>true</code> if transformer can be applied
     */
    protected abstract boolean canPerformTransformationImpl(String input);

    protected static boolean isVowel(char c) {
        return "aeiouAEIOU".contains(String.valueOf(c));
    }

    protected static boolean isConsonant(char c) {
        return "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ".contains(String.valueOf(c));
    }
}
