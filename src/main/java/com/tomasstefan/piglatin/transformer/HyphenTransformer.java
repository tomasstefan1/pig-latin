package com.tomasstefan.piglatin.transformer;

import com.tomasstefan.piglatin.transformer.ITextTransformer;

/**
 * Split input by hyphens to separate words and perform transformation by provided transformer on every word separately.
 * <br>
 * If there is no hyphen present in the word it will call {@link ITextTransformer#transform(String)} on whole input.
 */
class HyphenTransformer implements ITextTransformer {
    private final ITextTransformer transformer;

    HyphenTransformer(ITextTransformer transformer) {
        this.transformer = transformer;
    }

    @Override
    public String transform(String input) {
        if (input == null || input.isBlank()) throw new IllegalArgumentException("Input cannot be null");
        if (canPerformTransformation(input)) {
            return performTransformation(input);
        }
        return transformer.transform(input);
    }

    private String performTransformation(String input) {
        StringBuilder builder = new StringBuilder();
        String[] substrings = input.split("-");
        for (int i = 0; i < substrings.length; i++) {
            builder.append(transformer.transform(substrings[i]));
            if (i < substrings.length - 1) {
                builder.append("-");
            }
        }
        return builder.toString();
    }

    @Override
    public boolean canPerformTransformation(String input) {
        return input.contains("-");
    }
}
