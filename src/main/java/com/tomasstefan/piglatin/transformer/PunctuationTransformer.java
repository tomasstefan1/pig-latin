package com.tomasstefan.piglatin.transformer;

/**
 * <p>Creates map with positions and values of punctuation in provided input.</p>
 * <p>Use {@linkplain #getWithoutPunctuation()} to get word strip of punctuation and perform any other transformation on returned String</p>
 * <p>Call {@link #transform(String)} after all other transformations were performed and punctuation will be added back according to generated map from back to front.</p>
 * <br/>
 * <p>Punctuation character is every character that it's type <i>{@link Character#getType(int)}</i> will return one of following: <br/>
 * {@linkplain Character#CONNECTOR_PUNCTUATION},
 * {@linkplain Character#DASH_PUNCTUATION},
 * {@linkplain Character#END_PUNCTUATION},
 * {@linkplain Character#FINAL_QUOTE_PUNCTUATION},
 * {@linkplain Character#INITIAL_QUOTE_PUNCTUATION},
 * {@linkplain Character#OTHER_PUNCTUATION},
 * {@linkplain Character#START_PUNCTUATION}
 * <br/>
 * or if <code>character == 96</code></p>
 */
class PunctuationTransformer implements ITextTransformer {
    private final String input;
    int[] punctuationMap;

    PunctuationTransformer(String input) {
        this.input = input;
        punctuationMap = input.chars().map(character -> isPunctuation(character) ? character : -1).toArray();
    }

    private boolean isPunctuation(int character) {
        if (character == 96) return true; // for character `
        switch (Character.getType(character)) {
        case Character.CONNECTOR_PUNCTUATION:
        case Character.DASH_PUNCTUATION:
        case Character.END_PUNCTUATION:
        case Character.FINAL_QUOTE_PUNCTUATION:
        case Character.INITIAL_QUOTE_PUNCTUATION:
        case Character.OTHER_PUNCTUATION:
        case Character.START_PUNCTUATION:
            return true;
        default: return false;
        }

    }

    /**
     * @return input string striped of every punctuation character
     */
    String getWithoutPunctuation() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < punctuationMap.length; i++) {
            if (punctuationMap[i] == -1) {
                builder.append(input.charAt(i));
            }
        }
        return builder.toString();
    }

    @Override
    public String transform(String input) {
        StringBuilder builder = new StringBuilder();
        int j = input.length() -1;
        for (int i = 1; i <= punctuationMap.length; i++) {
            int character = punctuationMap[punctuationMap.length - i];
            if (character != -1) {
                builder.append(((char)character));
            } else if (j >= 0) {
                builder.append(input.charAt(j--));
            }
        }
        while (j >= 0) {
            builder.append(input.charAt(j--));
        }

        return builder.reverse().toString();
    }
}
