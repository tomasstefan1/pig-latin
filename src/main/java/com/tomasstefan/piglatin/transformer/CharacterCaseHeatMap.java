package com.tomasstefan.piglatin.transformer;

class CharacterCaseHeatMap implements ITextTransformer {
    int[] heatMap;

    /**
     * Creates upper/lower case heat map from provided input
     * @param input from which upper/lower case map is created
     */
    CharacterCaseHeatMap(String input) {
        if (input == null) throw new IllegalArgumentException("input cannot be null");
        heatMap = input.chars()
                .map(character -> Character.isAlphabetic(character) && Character.isUpperCase(character) ? Character.UPPERCASE_LETTER : Character.LOWERCASE_LETTER)
                .toArray();
    }

    /**
     * Iterate over input characters and make them upper or lower case based on heat map.
     *
     * @param input that will be decorated according to generated heat map
     * @return result changed according to heat map
     */
    @Override
    public String transform(String input) {
        char[] chars = input.toCharArray();
        for (int i = 0; i < (Math.min(heatMap.length, chars.length)); i++) {
            if (heatMap[i] == Character.UPPERCASE_LETTER) {
                chars[i] = Character.toUpperCase(chars[i]);
            } else {
                chars[i] = Character.toLowerCase(chars[i]);
            }
        }
        return String.valueOf(chars);
    }
}
